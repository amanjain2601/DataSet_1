let data = require("./EmployeeJson.js");

// store sum of salary and count of country in data given in key of object as array for a particular country
// where key of object is country name or location 

let DataOfCountry = data.reduce((AverageSalaryOfCountriesData, currentObject) => {
    if (AverageSalaryOfCountriesData[currentObject.location] == undefined)
        AverageSalaryOfCountriesData[currentObject.location] = [parseFloat(currentObject.salary.slice(1)), 1];
    else {
        let previouSumOfSalary = AverageSalaryOfCountriesData[currentObject.location][0];
        let previousCount = AverageSalaryOfCountriesData[currentObject.location][1];
        AverageSalaryOfCountriesData[currentObject.location] = [previouSumOfSalary + parseFloat(currentObject.salary.slice(1)), previousCount + 1];
    }

    return AverageSalaryOfCountriesData;
}, {})

DataOfCountry = Object.entries(DataOfCountry);

//now divide sum_of_salary/no of occurence for a particular country value stored as array

let AverageSalaryOfCountries = DataOfCountry.reduce((AverageSalaryOfCountries, currentObject) => {
    AverageSalaryOfCountries[currentObject[0]] = (currentObject[1][0] / currentObject[1][1]).toFixed(2);

    return AverageSalaryOfCountries;
}, {})

AverageSalaryOfCountries = JSON.stringify(AverageSalaryOfCountries, null, 2);
console.log("Average Salary of diffirent countries");
console.log(AverageSalaryOfCountries);






