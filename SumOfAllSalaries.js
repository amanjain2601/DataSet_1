let Data=require("./EmployeeJson.js");

let sumOfAllSalaries=Data.reduce((sumOfSalaries,currentObject)=>{
     sumOfSalaries=sumOfSalaries+parseFloat(currentObject.salary.slice(1));
     return sumOfSalaries;
},0.0);

console.log("Sum of all salary in the Data")
console.log(sumOfAllSalaries);