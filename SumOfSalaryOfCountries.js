let data = require("./EmployeeJson.js");

let countrySalarySum = data.reduce((countrySalarySum, currentObject) => {
    if (countrySalarySum[currentObject.location] == undefined)
        countrySalarySum[currentObject.location] = parseFloat(currentObject.salary.slice(1));
    else
        countrySalarySum[currentObject.location] += parseFloat(currentObject.salary.slice(1));

    return countrySalarySum;
}, {})

console.log("Sum of salary based on country");
countrySalarySum=JSON.stringify(countrySalarySum,null,2);
console.log(countrySalarySum);
